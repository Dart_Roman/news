<?php

namespace App;

use App\Contracts\HasRating;
use App\Traits\Rateable;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Image extends Model implements HasMedia, HasRating
{
    use HasMediaTrait, Rateable;

    protected $fillable = ['title','body','author_id'];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(100)
            ->height(100)
            ->sharpen(10);

        $this->addMediaConversion('full-size')
            ->greyscale()
            ->withResponsiveImages();
    }
}
