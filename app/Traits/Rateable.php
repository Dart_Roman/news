<?php

namespace App\Traits;

use App\UsersRate;

trait Rateable
{
    public function isRate()
    {
      if (!auth()->check()) {
          return false;
      }

      return UsersRate::where([
            'user_id' => auth()->user()->id,
            'post_id' => $this->id,
            'post_model' => self::class
          ])
          ->count();
    }

    public function getClass(): string
    {
        return self::class;
    }
}
