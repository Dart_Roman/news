<?php

namespace App\Contracts;

/**
 * Interface for model with rating
 */
interface HasRating
{
    public function isRate();
}
