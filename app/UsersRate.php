<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersRate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'post_id', 'post_model'
    ];
}
