<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNewsRequest;
use App\Http\Requests\RateRequest;
use App\Http\Services\RateService;
use App\News;
use App\Repositories\NewsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('create');
        $this->middleware('auth')->only('store');
        $this->middleware('auth')->only('rate');
    }

    /**
     * Show the list of news.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, NewsRepository $repository)
    {
        if ($request->has('search') && $request->input('search')) {
            $news = $repository->searchOnElasticsearch($request->input('search'));
            $ids = Arr::pluck($news['hits']['hits'], '_id');
            $newsList = News::whereIn('id', $ids)
                ->orderBy('created_at', 'desc')
                ->paginate(9);
        } else {
            $newsList = News::orderBy('created_at', 'desc')->paginate(9);
        }

        return view('news/index', [
            'newsList' => $newsList,
            'search' => $request->input('search'),
        ]);
    }

    /**
     * Show the form to create news.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('news/create');
    }

    /**
     * Save news.
     *
     * @return Boolean
     */
    public function store(StoreNewsRequest $request)
    {
        return News::create([
            'title' => $request->title,
            'body' => $request->body,
            'author_id' => $request->user()->id,
        ]);
    }

    /**
     * Show the news.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(News $news)
    {
        return view('news/show', [
            'news' => $news,
        ]);
    }

    /**
     * Rate the image.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return int
     */
    public function rate(Request $request, News $news): int
    {
        RateService::rate($news, $request->like);

        return $news->rate;
    }
}
