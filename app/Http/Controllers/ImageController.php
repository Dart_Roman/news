<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreImageRequest;
use App\Http\Requests\RateRequest;
use App\Http\Services\RateService;
use App\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('create');
        $this->middleware('auth')->only('store');
        $this->middleware('auth')->only('rate');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $imageList = Image::orderBy('created_at', 'desc')->paginate(3);

      return view('image/index', [
          'imageList' => $imageList,
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('image/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StoreImageRequest  $request
     *
     * @return \Illuminate\Http\Redirect
     */
    public function store(StoreImageRequest $request)
    {
        $image = Image::create([
            'title' => $request->title,
            'body' => $request->body,
            'author_id' => $request->user()->id,
        ]);

        if ($request->hasFile('image') && $request->file('image')->isValid())
        {
           $image->addMediaFromRequest('image')->toMediaCollection('images');
        }

        return redirect()->route('image.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        //
    }

    /**
     * Rate the image.
     *
     * @param  RateRequest  $request
     * @param  \App\Image  $image
     * @return int
     */
    public function rate(RateRequest $request, Image $image): int
    {
        RateService::rate($image, $request->like);

        return $image->rate;
    }
}
