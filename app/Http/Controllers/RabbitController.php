<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessTest;
use Illuminate\Http\Request;

class RabbitController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        ProcessTest::dispatch('hello')->onQueue('test');
        ProcessTest::dispatch('qwerty')->onQueue('sends');

        return view('home');
    }
}
