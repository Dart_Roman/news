<?php

namespace App\Http\Services;

use App\Contracts\HasRating;
use App\Traits\Rateable;
use App\UsersRate;

class RateService
{
    /**
     * Rate the post.
     *
     * @param  HasRating  $model
     * @param  bool  $isLike
     */
    public static function rate(HasRating $model, bool $isLike): void
    {
        if ($isLike) {
            self::like($model);
        } else {
            self::dislike($model);
        }
    }

    /**
     * Like the post.
     *
     * @param  HasRating  $model
     */
    protected static function like(HasRating $model): void
    {
        UsersRate::create([
          'user_id' => auth()->user()->id,
          'post_id' => $model->id,
          'post_model' => $model->getClass(),
        ]);

        $model->increment('rate');
    }

    /**
     * Dislike the post.
     *
     * @param  HasRating  $model
     */
    protected static function dislike(HasRating $model): void
    {
        UsersRate::where([
          'user_id' => auth()->user()->id,
          'post_id' => $model->id,
          'post_model' => $model->getClass(),
        ])->delete();

        $model->decrement('rate');
    }
}
