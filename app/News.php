<?php

namespace App;

use App\Contracts\HasRating;
use App\Traits\Searchable;
use App\Traits\Rateable;
use Illuminate\Database\Eloquent\Model;

class News extends Model implements HasRating
{
    use Rateable, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'body', 'author_id'
    ];
}
