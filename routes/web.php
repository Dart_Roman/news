<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/news', 'NewsController');
Route::put('/news/rate/{news}', 'NewsController@rate')->name('news.rate');

Route::resource('/image', 'ImageController');
Route::put('/image/rate/{image}', 'ImageController@rate')->name('image.rate');

Route::get('/rabbit', 'RabbitController@index')->name('rabbit');
