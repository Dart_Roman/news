@extends('layouts.app')

@section('content')

  <div class="container">
    <h1>Список новостей</h1>

    <form>
        <div class="input-group mb-3">
            <input
              type="search"
              name="search"
              class="form-control"
              placeholder="Найти новости"
              aria-label="Найти новости"
              aria-describedby="basic-addon2"
              value="{{ $search }}"
            >
            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit">Найти</button>
            </div>
        </div>
    </form>
    <div class="row">
      @foreach ($newsList as $news)
        <news-component
          :item="{{ $news }}"
          :is-rate="{{ $news->isRate() ? 1 : 0 }}"
          rate-url="{{ route('news.rate', $news->id) }}"
          url="{{ route('news.show', $news->id) }}"
          :is-auth="{{ auth()->check() ? 1 : 0 }}"
        ></news-component>
      @endforeach
    </div>

    <div class="float-left">
      {{ $newsList->links() }}
    </div>

    @auth
      <a class="btn btn-success float-right m-2" href="{{ route("news.create") }}">Создать новую статью</a>
    @endauth
  </div>

@endsection
