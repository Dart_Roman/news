@extends('layouts.app')

@section('content')

  <article>
    <div class="container">
      <h1 class="mt-5">{{ $news->title }}</h1>

      <div class="p-3 m-3 lead bg-white border"> {{ $news->body }}</div>
    </div>
  </article>

@endsection
