@extends('layouts.app')

@section('content')

  <div class="container">
    <h1>Изображения</h1>

    <div class="row">
      @foreach ($imageList as $image)
        <image-component
          :item="{{ $image }}"
          file="{{ $image->getFirstMediaUrl('images', 'thumb') }}"
          full="{{ $image->getFirstMediaUrl('images') }}"
          rate-url="{{ route('image.rate', $image->id) }}"
          :is-rate="{{ $image->isRate() ? 1 : 0 }}"
          :is-auth="{{ auth()->check() ? 1 : 0 }}"
        ></image-component>
      @endforeach
    </div>

    <div class="float-left">
      {{ $imageList->links() }}
    </div>

    @auth
      <a class="btn btn-success float-right m-2" href="{{ route("image.create" )}}">Новый пост</a>
    @endauth
  </div>

@endsection
